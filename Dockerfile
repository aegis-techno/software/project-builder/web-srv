FROM adoptopenjdk:11-jre-openj9

LABEL authors="Aegis <boris.bodin@gmail.com>"
LABEL vendor="Aegis-Techno"
LABEL tools="project-builder"

# Copy files
RUN mkdir /opt/app
COPY target/project-builder-0.0.1-SNAPSHOT.jar /opt/app

# Run jar
CMD ["java", "-jar", "/opt/app/project-builder-0.0.1-SNAPSHOT.jar"]

