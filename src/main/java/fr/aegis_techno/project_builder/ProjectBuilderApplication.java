package fr.aegis_techno.project_builder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectBuilderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectBuilderApplication.class, args);
	}

}
